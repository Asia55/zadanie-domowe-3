package pl.codementors.comics;

import java.io.*;
import java.util.*;

/**
 * Represents comics library. Contains inner collection with comics, exposes methods for adding and removing comics.
 * Allows for changing covers of all contained comics.
 *
 * @author psysiu
 */
public class ComicsLibrary {

    /**
     * Set of comics contained in the library.
     */
    private Set<Comic> comics = new HashSet<>();

    /**
     * @return All comics in the library. The returned collection is unmodifiable so it can not be changed
     * outside the library.
     */
    public Collection<Comic> getComics() {
        return Collections.unmodifiableSet(comics);
    }

    /**
     * Adds comic to the library. If comic is already in the library does nothing. If comic is null does nothing.
     *
     * @param comic Comic to be added to the library.
     */
    public void add(Comic comic) {
        if (comic != null) {
            comics.add(comic);
        }
    }

    /**
     * Removes comic from the library. If comics is not present in the library does nothing.
     *
     * @param comic Comic to be removed from the library.
     */
    public void remove(Comic comic) {
        comics.remove(comic);
    }

    /**
     * Changes covers of all comics in the library.
     *
     * @param cover Cover type for all comics in the library.
     */
    public void changeCovers(Comic.Cover cover) {

        for (Comic c : comics) {
            c.setCover(cover);
        }
    }

    /**
     * @return All authors of all comics in the library. Each author is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getAuthors() {
        Set<String> authors = new HashSet<>();
        for (Comic c : comics) {
            authors.add(c.getAuthor());
        }

        return Collections.unmodifiableSet(authors);
    }

    /**
     * @return All series of all comics in the library. Each series is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getSeries() {
        Set<String> series = new HashSet<>();
        for (Comic c : comics) {
            series.add(c.getSeries());
        }
        return series;
    }

    /**
     * Loads comics from file. Method uses FileReader and Scanner for reading comics from file.
     * <p>
     * The file structure is:
     * number_of_comics (one line with one number, nextInt())
     * comics title (one line with spaces, nextLine())
     * comics author (one line with spaces, nextLine())
     * comics series (one line with spaces, nextLine())
     * cover (one line with one word, next())
     * publish_month (one line with one number, nextInt())
     * publish_year (one line with one number, nextInt())
     * <p>
     * The proper sequence for reading file is to call nextInt(); skip("\n"); to read number of comics.
     * Then in loop call nextLine(); nextLine(); nextLine(), next(); nextInt(); nextInt(); skip("\n").
     * <p>
     * If file does not exists, or is directory, or can not be read, method just ignores it and does nothing.
     *
     * @param file File from which comics will be loaded.
     */
    public void load(File file) {


        if (!file.exists()) {
            return;
        }
        if (file.isDirectory() || !file.canRead()) {
            return;
        }
        if (!file.isFile()) {
            return;
        }


        try (FileReader fr = new FileReader(file);
             Scanner scanner = new Scanner(fr)) {

            int number_of_comics = scanner.nextInt();

            scanner.skip("\n");
            for (int i = 0; i < number_of_comics; i++) {

                String comics_title = scanner.nextLine();
                scanner.skip("\n");
                String comics_author = scanner.nextLine();
                scanner.skip("\n");
                String comics_series = scanner.nextLine();
                scanner.skip("\n");
                String cover = scanner.next();
                scanner.skip("\n");
                int publish_month = scanner.nextInt();
                scanner.skip("\n");
                int publish_year = scanner.nextInt();
                scanner.skip("\n");

                Comic comic = new Comic(comics_title, comics_author, comics_series, Comic.Cover.valueOf(cover), publish_year, publish_month);

                comics.add(comic);

            }

        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();

        } catch (NoSuchElementException ex) {
            return;
        }
    }


    /**
     * Counts all comics with the same provided series name.
     *
     * @param series Name of the series for which comics will be counted.
     * @return Number of comics from the same provided series.
     */
    public int countBySeries(String series) {

        int seriesCounter = 0;
        for (Comic comic : comics) {
            if (comic.getSeries().equals(series)) {
                seriesCounter++;
            }
        }
        return seriesCounter;
    }

    /**
     * Counts all comics with the same provided author.
     *
     * @param author Author for which comics will be counted.
     * @return Number of comics with the same author.
     */
    public int countByAuthor(String author) {
        int authorCounter = 0;
        for (Comic comic : comics) {
            if (comic.getAuthor().equals(author)) {
                authorCounter++;
            }
        }
        System.out.println(authorCounter);
        return authorCounter;

    }

    /**
     * Counts all comics with the same provided publish hear.
     *
     * @param year Publish year for which comics will be counted.
     * @return Number of comics from the same provided publish year.
     */
    public int countByYear(int year) {
        int yearCounter = 0;

        for (Comic comic : comics) {
            if (comic.getPublishYear() == year) {
                yearCounter++;
            }
        }
        return yearCounter;
    }

    /**
     * Counts all comics with the same provided publish year and month.
     *
     * @param year  Publish year for which comics will be counted.
     * @param month Publish month for which comics will be counted.
     * @return Number of comics from the same provided publish year and month.
     */
    public int countByYearAndMonth(int year, int month) {
        int yearAndMonthCounter = 0;


        for (Comic comic : comics) {
            if (comic.getPublishYear() == year && comic.getPublishMonth() == month) {
                yearAndMonthCounter++;
            }
        }
        return yearAndMonthCounter;
    }

    /**
     * Removes all comics with publish year smaller than the provided year. For the removal process
     * method uses iterator.
     *
     * @param year Provided year.
     */
    public void removeAllOlderThan(int year) {

        Iterator<Comic> iterator = comics.iterator();

        while (iterator.hasNext()) {
            Comic comic = iterator.next();

            if (comic.getPublishYear() < year) {
                iterator.remove();
            }
        }
    }

    /**
     * Removes all comics written by the specified author. For the removal process method uses iterator.
     *
     * @param author Provided author.
     */
    public void removeAllFromAuthor(String author) {
        Iterator<Comic> iterator = comics.iterator();

        while (iterator.hasNext()) {
            Comic comic = iterator.next();
            if (comic.getAuthor().equals(author)) {
                iterator.remove();
            }
        }

    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping author->comics. Map keys are names of the authors (String) present in the library. Map values are
     * collection (e.g.: HashSet<Comic>) of comics for specified author.
     */
    public Map<String, Collection<Comic>> getAuthorsComics() {

        Map<String, Collection<Comic>> comicsAuthors = new HashMap<>();
        Collection<String> authors = getAuthors();


        for (String author : authors) {
            comicsAuthors.put(author, new HashSet<>());
        }

        for (Comic comic : comics){
            Collection<Comic> comicCollection = comicsAuthors.get(comic.getAuthor());
            comicCollection.add(comic);
        }

        return comicsAuthors;
    }


    /**
     * Creates specified map and returns it.
     *
     * @return Mapping publish year->comics. Map keys are publish year (Integer, generics can not be simple types
     * so instead int the Integer is used) present in the library. Map values are collection (e.g.: HashSet<Comic>)
     * of comics for specified author.
     */
    public Map<Integer, Collection<Comic>> getYearsComics() {
        throw new UnsupportedOperationException();
    }
}
